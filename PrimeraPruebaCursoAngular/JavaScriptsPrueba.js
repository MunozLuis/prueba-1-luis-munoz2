let json = {

    "nombre"    : "",
    "apellidoP" : "",
    "apellidoM" : "",
    "correo"    : "",
    "razon"     : "",
    "consulta"  : ""
}

var arrayLetra = ["a","b","c","d","e","f","g","h","i",
                  "j","k","l","m","n","ñ","o","p","q",
                  "r","s","t","u","v","w","x","y","z",
                  "A","B","C","D","E","F","G","H","I",
                  "J","K","L","M","N","Ñ","O","P","K",
                  "R","S","T","U","V","W","X","Y","Z"];

function listarFormulario(){

    let mensaje = document.getElementById("jsonModal");
    mensaje.classList.remove("hide");

    json.nombre     =   document.getElementById("nombre").value;
    json.apellidoP  =   document.getElementById("apellidoP").value;
    json.apellidoM  =   document.getElementById("apellidoM").value;
    json.correo     =   document.getElementById("correo").value;
    json.razon      =   document.getElementById("razon").value;
    //json.consulta   =   document.getElementsById("consulta").value;

    if((json.nombre == "") || (json.apellidoP == "") || (json.correo == "")){
        
        alert("el campo nombre, apellido paterno o correo están vacíos");
        return false;
        
    }
    

    for(let i = 0; i < json.razon.length; i++){
        if(json.razon[i] !== json.razon[0]){   
            alert("seleccione una razon");
            return false;
        }
    }
    

    
    /* for(let i = 0; i < json.nombre.length; i++){    
            if(json.nombre[i] !== arrayLetra[i]){
                alert("El nombre contiene números");
                return false;
            }
        }
    */

    /*   for(let i = 0; i < json.apellidoP.length; i++){    
            if(json.apellidoP[i] !== arrayLetra[i]){
                alert("El nombre contiene números");
                return false;
            }
        }
    */
   /*   for(let i = 0; i < json.nombre.length; i++){    
            if(json.nombre[i] !== arrayLetra[i]){
                alert("El nombre contiene números");
                return false;
            }
        }
    */

    /*if((json.consulta.length > 500) || (json.consulta.length < 1)){
        alert("la consulta debe contener una descripción que no supere los 500 carácteres");
       
    }
    */

    mensaje.innerHTML = JSON.stringify(json);
    return true;
     
}